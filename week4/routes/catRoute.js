"use strict";
// catRoute

const router = require("express").Router();
const catController = require("../controllers/catController.js");
const multer = require("multer");
const upload = multer({ dest: "./uploads/" });

router
  .route("/")
  .get(catController.cat_list_get)
  .post(upload.single("cat"), (req, res) => {
    res.send("uploaded");
  });
router.get("/:id", catController.cat_get);

module.exports = router;
