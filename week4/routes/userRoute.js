"use strict";

const router = require("express").Router();
const userController = require("../controllers/userController");

router
  .route("/")
  .get(userController.user_list_get)
  .post((req, res) => {
    res.json(res.body);
  });
router.get("/:id", userController.user_get);

module.exports = router;
