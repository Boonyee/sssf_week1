"use strict";
const passport = require("passport");
const Strategy = require("passport-local").Strategy;
const userModel = require("../models/userModel.js");
const passportJWT = require("passport-jwt");
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

// local strategy for username password login
passport.use(
  new Strategy(
    {
      usernameField: "username",
      passwordField: "password",
    },
    async (email, password, done) => {
      try {
        const user = await userModel.getUserLogin(email);
        console.log("Local strategy", user); // result is binary row
        if (user === undefined) {
          return done(null, false, { message: "Incorrect email." });
        }
        if (user.password !== password) {
          return done(null, false, { message: "Incorrect password." });
        }
        return done(null, user, { message: "Logged In Successfully" }); // use spread syntax to create shallow copy to get rid of binary row type
      } catch (err) {
        return done(err);
      }
    }
  )
);

// TODO: JWT strategy for handling bearer token
passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      secretOrKey: "your_jwt_secret",
    },
    async (jwtPayload, done) => {
      try {
        const user = await userModel.getUserLogin(jwtPayload.email);
        console.log("JWT strategy", user);
        if (user === undefined) {
          return done(null, false, { message: "User not found." });
        }
        return done(null, user, { message: "Logged In Successfully" });
      } catch (err) {
        return done(err);
      }
    }
  )
);

module.exports = passport;
