"use strict";
const express = require("express");
const app = express();
const port = 3000;
const catRoute = require("./routes/catRoute.js");
const userRoute = require("./routes/userRoute");
const authRoute = require("./routes/authRoute.js");
const cors = require("cors");
const passport = require("./utils/pass.js");

app.use(express.static("public_html"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/cat", passport.authenticate("jwt", { session: false }), catRoute);
app.use("/user", passport.authenticate("jwt", { session: false }), userRoute);
app.use("/auth", authRoute);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
