"use strict";
// catController
const catModel = require("../models/catModel");

const cats = catModel.cats;

const cat_list_get = (req, res) => {
  res.json(cats);
};

const cat_get = (req, res) => {
  const result = cats.filter((model) => {
    return model.id == req.params.id;
  });
  res.json(result);
};

module.exports = {
  cat_list_get,
  cat_get,
};
